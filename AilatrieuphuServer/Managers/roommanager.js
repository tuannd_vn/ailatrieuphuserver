module.exports = RoomManager;

var Room = require('../Entities/room.js');

function RoomManager() {
	this.listRoom = [];
};

RoomManager.prototype.createListRoom = function(number) {
	//console.log('create list room');
	for (var i = 0; i < number; i++) {
		var room = new Room("Phong " + (i+1), i);
		this.addRoom(room);
	}
}

RoomManager.prototype.addRoom = function(room) {
	if (this.listRoom!=undefined) {
		this.listRoom.push(room);		
	};
};

RoomManager.prototype.getListRoomJson = function() {
	return JSON.stringify(this.listRoom);
};


