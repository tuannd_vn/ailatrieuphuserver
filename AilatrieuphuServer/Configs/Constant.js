module.exports = Constant;

function Constant() { }
//---------------------system----------------------
global.client_connect = 'connection';
global.client_disconnect = 'disconnect';
global.client_message = 'client.message';
//---------------------client custom----------------------
global.client_login = 'client.login';
global.client_signup = 'client.signup';
global.client_choose_game = 'client.choose.game';
global.client_choose_room = 'client.choose.room';
global.client_invite_friend = 'client.invite.friend';
global.client_chat = 'client.chat.message';

global.client_get_question = 'client.get.question';
global.client_answer = 'client_answer';

//---------------------server custom------------------
global.server_welcome_client = 'server.welcome.client';
global.server_send_question = 'server.send.question';