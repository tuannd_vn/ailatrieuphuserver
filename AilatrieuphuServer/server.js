var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var constant = require('./Configs/Constant.js');

var RoomManager = require('./Managers/roommanager.js');
var UserManager = require('./Managers/usermanager.js');
var roomMng = new RoomManager();
var userMng = new UserManager();

var Message = require('./Entities/message.js');
var Room = require('./Entities/room.js');

var Connection = require('./Connectors/connection.js');
var conn = new Connection();
var SignUp = require('./Components/SignUp.js');
var signup = new SignUp();
var Login = require('./Components/Login.js');
var login = new Login();
var Question = require('./Entities/question.js');
var AppUltis = require('./Ultis/appultis.js');
var appultis = new AppUltis();

io.on(global.client_connect, function (client) {
    console.log('a user connected');
    userMng.initUser(client);
    
    client.on(global.client_disconnect, function () {
        userMng.removeUserById(client.id);
        console.log('a user disconnected: ' + client.id);
    });
    
    client.on(global.client_message, function (msg) {
        var jsonData = JSON.parse(msg);
        var command = jsonData['command'];
        switch (command) {
            case global.client_signup:
                var username = jsonData['username'];
                var password = jsonData['password'];
                clientSignup(username, password);
                break;
            case global.client_login:
                var username = jsonData['username'];
                var password = jsonData['password'];
                clientLogin(username, password);
                //---
                userMng.setUserName(client.id, msg);
                client.emit('server.list.room', roomMng.getListRoomJson());
                break;
            case global.client_choose_game:
                break;
            case global.client_choose_room:
                clientChooseRoom(msg);
                break;
            case global.client_chat:
                clientChat(client);
                break;
            case global.client_get_question:
                var level = jsonData['level'];
                break;
            case global.client_answer:
                var answer = jsonData['answer'];
                var questionId = jsonData['questionId'];
                break;
            default:
                break;
        }
    });
});

function clientSignup(username, password) { 
    signup.excuteSignup(username, password);
}

function clientLogin(username, password) { 
    login.excuteLogin(username, password);
}

function clientChat(client) { 
    var user = userMng.getUserById(client.id);
    if (user != undefined) {
        console.log(user.toString());
    };
    var jsonData = JSON.parse(data);
    var data = '{"Sender":"Anonymous", "Content":"' + jsonData['message'] + '"}';
    var listUserInRoom = userMng.getListUserByRoomId(user.getRoomId());
    userMng.sendMessageToListUser(io, global.client_chat, data, listUserInRoom);
}

function clientChooseRoom(data) {
    var roomId = JSON.parse(data)['roomId'];
    var user = userMng.getUserById(client.id);
    user.setRoom(roomId);
    
    var mess = new Message('Server', 'Welcome to room: ' + roomId);
    var listUserInRoom = userMng.getListUserByRoomId(roomId);
    
    userMng.sendMessageToListUser(io, global.server_welcome_client, mess.toString(), listUserInRoom);
}

function clientGetQuestion(client, level) 
{
    var query = 'SELECT * FROM ninequestions where level = ? ORDER BY rand() LIMIT 1';
    conn.excuteUpdate(query, [level], function (row) { 
        var question = new Question();
        question.loadQuestionBase64String(row);
        console.log(question);
        client.emit(global.server_send_question, question.toString());
    });
}

function clientAnswer(questionId, answer) {
    var query = 'SELECT casea FROM `ninequestions` where id = ?';
    conn.excuteUpdate(query, [questionId], function (row) {
        var casea = row[0]['casea'];
        var encode = appultis.decodeBase64Special(casea);
        var answerEncode = appultis.decodeBase64Special(answer);
        if (encode == answerEncode) {
            console.log('right answer');
        }
        else { 
            console.log('wrong answer');
        }
    });
}

http.listen(2015, function () {
    roomMng.createListRoom(5);
    console.log('listen on port 2015...');
    //clientGetQuestion(undefined, 1);
    clientAnswer(1, 'VspeOgbmc=');
});

