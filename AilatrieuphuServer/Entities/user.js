module.exports = User;

function User(socket, name){
	this.Name = name;
    this.SessionId = socket.id;
    this.Connected = true;
}

User.prototype.setName = function(name) {
	this.Name = name;
};

User.prototype.getSessionId = function() {
	return this.SessionId;
};

User.prototype.getRoomId = function() {
	return this.RoomId;
};

User.prototype.setRoom = function(roomId) {
	this.RoomId = roomId;
};

User.prototype.toString = function() {
	return this.Name.toString() + ' : '+ this.RoomId.toString();
};

User.prototype.isConnected = function () {
    return this.Connected;
}