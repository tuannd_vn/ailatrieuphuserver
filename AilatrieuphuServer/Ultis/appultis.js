﻿module.exports = AppUltis;

function AppUltis() { }

AppUltis.prototype.decodeBase64Special = function(dataEncode) {
    var res = dataEncode.slice(0, 2) + dataEncode.slice(4, dataEncode.length);
    return res;
}

AppUltis.prototype.decodeBase64 = function (dataEncode) {
    var res = new Buffer(this.decodeBase64Special(dataEncode), 'base64').toString('utf-8');
    return res;
}

AppUltis.prototype.encodeBase64 = function (plainText) {
    var res = new Buffer(plainText).toString('base64');
    return res;
}